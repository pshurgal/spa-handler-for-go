package spa

import (
	"io/fs"
	"net/http"
	"os"
	"strings"
)

type Handler struct {
	fileSystem http.FileSystem
	fileServer http.Handler
}

func NewHandler(fs fs.FS) Handler {
	httpFS := http.FS(fs)
	return Handler{
		fileSystem: httpFS,
		fileServer: http.FileServer(httpFS),
	}
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	filepath := r.URL.Path
	if filepath != "/" {
		filepath = strings.TrimSuffix(r.URL.Path, "/")
	}

	file, err := h.fileSystem.Open(filepath)
	if err != nil {
		if os.IsNotExist(err) {
			defer func(old string) {
				r.URL.Path = old
			}(r.URL.Path)

			r.URL.Path = "/"

			h.fileServer.ServeHTTP(w, r)
			return
		}

		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	defer file.Close()

	h.fileServer.ServeHTTP(w, r)
}
