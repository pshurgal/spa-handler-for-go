import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import("@/views/HomePageView.vue")
    },
    {
      path: '/firstPage',
      name: 'First Page',
      component: () => import('../views/FirstPageView.vue')
    },
    {
      path: '/secondPage',
      name: 'Second Page',
      component: () => import('../views/SecondPageView.vue')
    }
  ]
})

export default router
