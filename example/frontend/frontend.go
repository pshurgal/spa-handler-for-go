package frontend

import (
	"embed"
	"io/fs"
)

//go:embed all:dist
var staticFiles embed.FS

func NewFileSystem() fs.FS {
	fs, _ := fs.Sub(staticFiles, "dist")
	return fs
}
