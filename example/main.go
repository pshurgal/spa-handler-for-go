package main

import (
	"fmt"
	"net/http"

	spa "gitlab.com/pshurgal/spa-handler-for-go"
	frontend "gitlab.com/pshurgal/spa-handler-for-go/example/frontend"
)

func main() {
	mux := http.NewServeMux()
	spa := spa.NewHandler(frontend.NewFileSystem())

	mux.Handle("GET /", &spa)
	mux.HandleFunc("GET /api/helloWorld", helloWorld)

	http.ListenAndServe(":8080", mux)
}

func helloWorld(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "Hello, World!")
}
